<?php

/**
 * Created by PhpStorm.
 * User: miltonleon
 * Date: 12/28/16
 * Time: 2:04 AM
 */
use Phalcon\Mvc\Controller;

/**
 * Class TestController
 */
class TestController extends Controller
{
    /**
     * Get.
     *
     * @param $token
     */
    public function getAction($token)
    {
        echo '<h1>GET...</h1>  ' . $token;
    }

    /**
     * Post
     */
    public function scrapAction($token)
    {
	    echo '<h1>POST...</h1> ' . $token;
    }
}