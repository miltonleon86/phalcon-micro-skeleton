<?php

/**
 * Created by PhpStorm.
 * User: miltonleon
 * Date: 12/28/16
 * Time: 3:00 AM
 */
class Config
{

	/**
	 * @return array
	 */
	public static function registerDirs()
	{
		return array(
			__DIR__ . "/../models/",
			__DIR__ . "/../controllers/",
			__DIR__ . "/../helpers/",
		);
	}

	/**
	 * @return array
	 */
	public static function registerNamespaces()
	{
		return array(
			"phalconmicro\\controllers" => __DIR__ . "/../controllers/",
			"phalconmicro\\models"      => __DIR__ . "/../models/",
			"phalconmicro\\helpers"     => __DIR__ . "/../helpers/",
		);
	}

	/**
	 * //TODO Return connection depending for current env.
	 *
	 * @return array
	 */
	public static function setDataBase()
	{
		return array(
			"host"     => "localhost",
			"username" => "root",
			"password" => "testing",
			"dbname"   => "scraper",
		);
	}
}