<?php
/**
 * Created by PhpStorm.
 * User: miltonleon
 * Date: 12/28/16
 * Time: 12:47 AM
 */

use Phalcon\Mvc\Micro;
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;

/** Composer, Config and Routes */
//require_once 'vendor/autoload.php';
require_once 'configs/Config.php';

$app = new Micro();
$di = new FactoryDefault();

/** Loader Object */
$loader = new Loader();

/** Register Directories */
$loader->registerDirs(Config::registerDirs())->register();

/** Register Namespaces */
$loader->registerNamespaces(Config::registerNamespaces());

/** Setting DB Using Lazy Loading */
$di->set('db', function () {
        return new MysqlAdapter(Config::setDataBase());
    }
);

/** Setting test Using Lazy Loading */
$di->set('test', function () {
        return new TestController();
    }
);

$app->get('/get/{token}', array(
        $di->get('test'), 'getAction')
);

$app->post('/scrap/{token}', array(
        $di->get('test'), 'scrapAction')
);

/** End */

$app->handle();

